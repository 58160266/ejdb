process.env.TZ = 'Asia/Bangkok';
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var collection = "account" ;

var EJDB = require("ejdb");
//Open zoo DB
var jb = EJDB.open('acc',EJDB.JBOWRITER | EJDB.JBOCREAT);
/*
var data1 = {
      "ACC_ID" : "ACC001",
      "ACC_Name" : "Panumat Makaew",
      "balance" : "5000",
      "DateOp" : new Date()
  }
  var data2 = {
        "ACC_ID" : "ACC002",
        "ACC_Name" : "Patcharapon Pimpet",
        "balance" : "400",
        "DateOp" : new Date()
    }
  var data3 = {
        "ACC_ID" : "ACC003",
        "ACC_Name" : "Korakoch Patcharapon",
        "balance" : "5",
        "DateOp" : new Date()
    }
jb.save(collection, [data1,data2,data3]);
*/
main();


function main() {
  rl.question(
      '[0] find account \n'+
      '[1] insert account \n'+
      '[2] edit account \n' +
      '[3] delete account \n'+
      '[4] deposit money\n'+
      '[5] withdraw money \n'+
      '[6] transfer money \n'+
      '[All] show all account \n'+
      '[exit] exit \n' +
      'input number : '
      , (answer) => {
        if(answer === "exit"){
          jb.close();
          process.exit();
        }else if(answer == '0'){
          search();
        }else if(answer == '1'){
          insertACC();
        }else if(answer == '2'){
          updateACC();
        }else if(answer == '3'){
          deleteACC();
        }else if(answer == '4'){
          deposit();
        }else if(answer == '5'){
          withdraw();
        }else if(answer == '6'){
	        transfer();
        }else if (answer === 'All' || answer === 'all'){
          list_All_Account();
        }else{
          main();
        }
  });
}

function list_All_Account() {
  jb.find(collection,{},{$orderby: {'ACC_ID': 1}},function(err, cursor, count) {
                if (err) {
                    console.error(err);
                    return;
                }
                //console.log("Found " + count + " parrots");
                if(cursor){
                  setHeadDisplay();
                  while (cursor.next()) {
                    setDisplay(cursor.field("ACC_ID"),cursor.field("ACC_Name"),cursor.field("balance"));
                  }
                  cursor.close();
                  main();
                }else {
                  //console.log("Don't have any account .");
                  exitList();
                }

            });
}

function exitList(){
  rl.question("Don't have any account . ( [enter] exit)",(input)=>{
    main();
  });
}

function setDisplay(acc_id,acc_name,balance){
  var space_acc_id = 10;
  var space_acc_name = 30;
  var space_balance = 7 - balance.toString().length;
  for(var i=acc_id.length ; i <= space_acc_id ; i++){
    acc_id += ' ' ;
  }
  for(var i=acc_name.length ; i <= space_acc_name ; i++){
    acc_name += ' ' ;
  }
  for (var i = 0; i < space_balance ; i++) {
    balance = ' '+ balance ;
  }
  console.log("| "+acc_id+"| "+acc_name+" | "+balance+" Baht  |");
  console.log("+------------+---------------------------------+---------------+");
}
function setHeadDisplay() {
  console.log("+------------+---------------------------------+---------------+");
  console.log("|   ACC_ID   |              ACC_Name           |    Balance    |");
  console.log("+------------+---------------------------------+---------------+");
}


function insertACC() {
  var num = 0;
  var acc =[collection];
  console.log("ACC_ID : input =>");
  rl.on('line',(data)=>{
    acc.push(data);
    switch (num) {
      case 0: console.log("ACC_Name : input => "); break;
      case 1: console.log("balance : input => "); break;
      default:
    }
    num++;
    if(num==3){
      acc.push(new Date());
      console.log("-----------------Insert Data-------------");
      console.log("  ACC_ID   : "+acc[1]);
      console.log("  ACC_Name : "+acc[2]);
      console.log("  Balance  : "+acc[3]);
      console.log("  DateOp   : "+acc[4]);
      console.log("-----------------------------------------");
      confirm_insert(acc);
    }
  });
}

function confirm_insert(acc) {
  rl.question('( [Y] insert data , [N] cancle) : ',(answer)=>{
    if(answer=='Y'||answer=='y'){
      insertInEJDB(acc);
      var data = findByACC_ID(acc[1]);
        console.log("-----------Insert Data Sucess------------");
        console.log("  ACC_ID   : "+data['ACC_ID']);
        console.log("  ACC_Name : "+data['ACC_Name']);
        console.log("  Balance  : "+data['balance']);
        console.log("  DateOp   : "+data['DateOp']);
        console.log("-----------------------------------------");
      exitInsert();
    }else if (answer=='N' || answer=='n'){
      //exitMain();
      console.log("Cancle Data Success");
      exitInsert();
    }else {
      confirm_insert(acc);
    }
  });
}

function exitInsert(){
  rl.question("Finish ! ( [exit] go to main , [I] Insert ): ",(input)=>{
      if(input==='exit'){
        exitMain();
      }else if(input == 'I' || input == 'i'){
        insertACC();
      }else {
        exitInsert();
      }
  });
}
function insertInEJDB(acc){
  var data = {
        "ACC_ID" : acc[1],
        "ACC_Name" : acc[2],
        "balance" : acc[3],
        "DateOp" : acc[4]
    }
  jb.save(acc[0] , [data]);
}


function exitMain() {
    main();
}

function search() {
  rl.question("What ACC_ID do you find ? ( [exit] = go to main ): ",(number)=>{
    var acc_id = number ;
    if(number === "exit"){
      exitMain();
    }else{
      //var data = jb.findOne(collection,{"ACC_ID" : acc_id });
      var data = findByACC_ID(acc_id);
      if(data != null){
        showData('search',data);
      }else{
        console.log("Not Found ACC_ID = "+number);
      }
      //find again
      search();
    }
  });
}


function updateACC(){
  rl.question("What ACC_ID do you want to update ? ( [exit] = go to main ): " , (number)=>{
      var acc_id = number;
      if(number === 'exit'){
        exitMain();
      }else {
        //var data_old = jb.findOne(collection,{"ACC_ID" : acc_id });
        var data_old = findByACC_ID(acc_id);
        if(data_old != null){
          showData('dataNow',data_old);
          var data =[collection];
          data.push(data_old['_id']);
          data.push(data_old['ACC_ID']);
          data.push(data_old['DateOp']);
          editACC(data);
        }else{
          console.log("Not Found ACC_ID "+acc_id);
          updateACC();
        }

      }
  })
}

function editACC(acc){
  var num = 0; //0:database 1:_id
  console.log("ACC_Name : edit to =>");
  rl.on('line',(data)=>{
    acc.push(data);
    if(num ==0 )
    console.log("Balance : edit to =>");
    num++;
    if(num==2){
      updateInEJDB(acc);
      exitUpate();
    }
  });
}

function exitUpate(){
  rl.question("Finish ! ( [exit] go to main , [U] Update ): ",(input)=>{
      if(input==='exit'){
        exitMain();
      }else if(input == 'U' || input == 'u'){
        updataACC();
      }else {
        exitTransfer();
      }
  });
}

function updateInEJDB(acc) {
  //console.log(acc);
  var data = {
        "_id"     : acc[1],
        "ACC_ID"  : acc[2],
        "ACC_Name": acc[4],
        "balance" : acc[5],
        "DateOp"  : acc[3]
    }
  jb.save(acc[0] , [data]);
  //var data = jb.findOne(collection,{"_id" : acc[1] });
  var data = findByObj_ID(acc[1]);
  showData('newData',data);
  updateACC();
}

function showData(src,data) {
  if(src === 'search'){
    console.log("-----------------------------------------");
  }else if (src === 'newData'){
    console.log("-----------------New Data----------------");
  }else if (src === 'dataNow'){
    console.log("---------------Data Now------------------");
  }
  console.log("  ACC_ID   : "+data['ACC_ID']);
  console.log("  ACC_Name : "+data['ACC_Name']);
  console.log("  Balance  : "+data['balance']);
  console.log("  DateOp   : "+data['DateOp']);
  console.log("-----------------------------------------");
}

function showDataDetail(data) {
  console.log("----------------Detail-------------------");
  console.log("  ACC_Name : "+data['ACC_Name']);
  console.log("  Balance  : "+data['balance']);
  console.log("-----------------------------------------");
}

function showDataDetailTransfer(data_s,data_d) {
  console.log("--------------Source Account-------------");
  console.log("  ACC_Name : "+data_s['ACC_Name']);
  console.log("  Balance  : "+data_s['balance']);
  console.log("----------Destination Account------------");
  console.log("  ACC_Name : "+data_d['ACC_Name']);
  console.log("  Balance  : "+data_d['balance']);
  console.log("-----------------------------------------");
}

function deleteACC() {
  rl.question("What ACC_ID do you want to delete ? ( [exit] = go to main ): " , (number)=>{
    var acc_id = number;
    if(number === 'exit'){
      exitMain();
    }else {
      var data_old = jb.findOne(collection,{"ACC_ID" : acc_id });
      if(data_old != null){
        showData('dataNow',data_old);
        confirm_remove(data_old);
      }else{
        console.log("Not Found ACC_ID "+acc_id);
        deleteACC();
      }
    }
  });
}

function removeInEJDB(data) {
  jb.remove(collection,data['_id']);
  console.log("Delete ACC_ID : "+data['ACC_ID']+" Sucess");
  deleteACC();
}

function confirm_remove(data) {
  rl.question('Delete This Data ( [Y] Yes , [N] No) : ',(answer)=>{
    if(answer == 'Y' || answer == 'y'){
      removeInEJDB(data);
    }else if(answer == 'N' || answer == 'n'){
      deleteACC();
    }else{
      confirm_remove(data);
    }
  });

}

function findByACC_ID(acc_id) {
  return jb.findOne(collection,{"ACC_ID" : acc_id });
}

function findByObj_ID(obj_id){
  return jb.findOne(collection,{"_id" : obj_id });
}

function deposit(){
  rl.question('What ACC_ID do you want to deposit money ? ( [exit] = go to main ): ',(number)=>{
    var acc_id = number ;
    if (number === "exit"){
    exitMain();
    }else {
      var data = findByACC_ID(acc_id);
      if(data != null){
        showDataDetail(data);
        rl.question('Deposit (baht): ',(num)=>{
          var money = num;
          confirm_deposit(data,money);
        })
      }else{
        console.log("Not Found ACC_ID :"+acc_id);
        deposit();
      }

      //console.log("deposit");
    }
  });
}

function confirm_deposit(data,money){
  rl.question("Deposit "+money+" baht to ACC_id : "+data['ACC_ID']+" ( [Y] Yes , [N] No) :",(answer)=>{
    if(answer == 'Y' || answer == 'y'){
      depositInEJDB(data,money);
    }else if(answer == 'N' || answer == 'n'){
      deposit();
    }else{
      confirm_deposit(data,money);
    }
  });
}

function depositInEJDB(data,money) {
  data['balance'] = parseInt(data['balance']) + parseInt(money) ;
  jb.save(collection , [data]);
  var detail = findByObj_ID(data['_id']);
  //console.log(detail);
  showDataDetail(detail);
  rl.question("Success! ( [exit] go to main ): ",(input)=>{
      exitMain();
  });
}


function withdraw(){
  rl.question('What ACC_ID do you want to withdraw money ? ( [exit] = go to main ): ',(number)=>{
    var acc_id = number ;
    if (number === "exit"){
    exitMain();
    }else {
      var data = findByACC_ID(acc_id);
      if(data != null){
        showDataDetail(data);
        rl.question('Deposit (baht): ',(num)=>{
          var money = num;
          confirm_withdraw(data,money);
        })
      }else{
        console.log("Not Found ACC_ID :"+acc_id);
        withdraw();
      }

      //console.log("withdraw");
    }
  });
}

function confirm_withdraw(data,money){
  rl.question("Deposit "+money+" baht to ACC_id : "+data['ACC_ID']+" ( [Y] Yes , [N] No) :",(answer)=>{
    if(answer == 'Y' || answer == 'y'){
      withdrawInEJDB(data,money);
    }else if(answer == 'N' || answer == 'n'){
      withdraw();
    }else{
      confirm_withdraw(data,money);
    }
  });
}

function withdrawInEJDB(data,money) {
  data['balance'] = parseInt(data['balance']) - parseInt(money) ;
  jb.save(collection , [data]);
  var detail = findByObj_ID(data['_id']);
  //console.log(detail);
  showDataDetail(detail);
  rl.question("Success! ( [exit] go to main ): ",(input)=>{
      exitMain();
  });
}

function transfer() {
  rl.question('What ACC_ID_Source do you want to transfer money ? ( [exit] = go to main ): ',(number)=>{
    var acc_id_source = number ;
    if(number==='exit'){
      exitMain();
    }else{
      rl.question('What ACC_ID_Destination do you want to transfer money ? ( [exit] = go to main ): ',(number)=>{
        var acc_id_destination = number ;
        if(number==='exit'){
          exitMain();
        }else{
          var message = checkTransfer_ACC_ID(acc_id_source,acc_id_destination);
          if(message != 0){
            decodeMessage(message);
            transfer();
          }else{
            decodeMessage(message);
            acc_id_source = findByACC_ID(acc_id_source);
            acc_id_destination = findByACC_ID(acc_id_destination);
            showDataDetailTransfer(acc_id_source,acc_id_destination);
            rl.question("Transfer (baht) : ",(number)=>{
              var money = number ;
              confirm_transfer(acc_id_source,acc_id_destination,money);
            });
          }
        }
      });
    }
  });
}

function decodeMessage(number) {
  switch (number) {
    case 0 : console.log("ACC_ID_Source and ACC_ID_Destination is exists , already to transfer .");
      break;
    case 1 : console.log("ACC_ID_Source and ACC_ID_Destination is not exists , try again .");
      break;
    case 2 : console.log("ACC_ID_Source  is not exists , try again .");
      break;
    case 3 : console.log("ACC_ID_Destination is not exists , try again .");
      break;
    default:
  }

}

function checkTransfer_ACC_ID(acc_id_source,acc_id_destination){
  var source = findByACC_ID(acc_id_source);
  var dest = findByACC_ID(acc_id_destination);
  if(source != null && dest != null){
    return 0;
  }else{
    if (source == null && dest == null ){
      return 1;
    }else if (source == null){
      return 2;
    }else if (dest == null){
      return 3;
    }
  }
}

function confirm_transfer(acc_id_source,acc_id_destination,money) {
  rl.question(
    "Transfer "+money+" baht form ACC_ID:" + acc_id_source['ACC_ID']+" to ACC_ID:" +
    acc_id_destination['ACC_ID']+" ( [Y] Yes , [N] No) :",(answer)=>{
      if(answer == 'Y' || answer == 'y'){
        transferInEJDB(acc_id_source,acc_id_destination,money);
      }else if (answer == 'N' || answer == 'n'){
        exitMain();
      }else{
        confirm_transfer(acc_id_source,acc_id_destination,money);
      }
    });
}

function transferInEJDB(acc_id_source,acc_id_destination,money) {
  acc_id_source['balance']      = parseInt(acc_id_source['balance']) - parseInt(money) ;
  acc_id_destination['balance'] = parseInt(acc_id_destination['balance']) +  parseInt(money) ;
  //save changed
  jb.save(collection , [acc_id_source,acc_id_destination]);
  //show detail in display
  var detail_source = findByObj_ID(acc_id_source['_id']);
  var detail_dest   = findByObj_ID(acc_id_destination['_id']);
  showDataDetailTransfer(detail_source,detail_dest);
  exitTransfer();
}

function exitTransfer() {
  rl.question("Success! ( [exit] go to main , [T] Transfer ): ",(input)=>{
      if(input==='exit'){
        exitMain();
      }else if(input == 'T' || input == 't'){
        transfer();
      }else {
        exitTransfer();
      }
  });
}
